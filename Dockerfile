#
# Glances Dockerfile for Raspberry Pi
#
# https://github.com/nicolargo/glances
# https://hub.docker.com/r/resin/raspberry-pi-alpine-python/
#

# Pull base image.
FROM resin/raspberry-pi-alpine-python:latest

ARG VERSION_TAG=v3.0.2

# Install Glances (develop branch)
RUN apk add --no-cache --virtual .build_deps \
	gcc \
	musl-dev \
	linux-headers \
	&& pip install 'psutil>=5.4.7,<5.5.0' bottle==0.12.13 \
	&& apk del .build_deps \
    && apk add --no-cache git \
    && git clone https://github.com/nicolargo/glances.git \
    && cd glances \
    && git checkout -b ${VERSION_TAG} tags/${VERSION_TAG}

# Define working directory.
WORKDIR /glances

# EXPOSE PORT (For Web UI & XMLRPC)
EXPOSE 61208 61209

# Define default command.
CMD python -m glances -C /glances/conf/glances.conf $GLANCES_OPT